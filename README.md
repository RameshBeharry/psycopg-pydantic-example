# Pyscopg + Pydantic Example

This example demonstrates how to use Pydantic models along with
Pyscopg to automatically validate SQL queries with Pydantic.

Here's the official documentation that explains this.

https://www.psycopg.org/psycopg3/docs/advanced/typing.html#example-returning-records-as-pydantic-models
