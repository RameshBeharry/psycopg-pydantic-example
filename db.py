import dotenv
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
import os
from pydantic import BaseModel
from rich import print

# Pydantic Models for our DB queries


class StudentResult(BaseModel):
    studentid: int
    studentname: str


class StudentsWithCourseResult(BaseModel):
    studentname: str
    coursename: str


dotenv.load_dotenv()

db_url = os.getenv("DATABASE_URL")
if not db_url:
    raise Exception("No DATABASE_URL set")

pool = ConnectionPool(db_url, max_size=10)


def initialize_db():
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
            -- Create Courses table
            CREATE TABLE IF NOT EXISTS Courses (
                CourseID INT PRIMARY KEY,
                CourseName VARCHAR(50)
            );

            -- Create Students table without CourseID
            CREATE TABLE IF NOT EXISTS Students (
                StudentID INT PRIMARY KEY,
                StudentName VARCHAR(50)
            );

            -- Create Enrollments table for many-to-many relationship
            CREATE TABLE IF NOT EXISTS Enrollments (
                StudentID INT,
                CourseID INT,
                PRIMARY KEY (StudentID, CourseID),
                FOREIGN KEY (StudentID) REFERENCES Students(StudentID),
                FOREIGN KEY (CourseID) REFERENCES Courses(CourseID)
            );
            """
            )

        with conn.cursor() as cur:
            cur.execute(
                """
            -- Because we don't want to keep inserting duplicate data...
            TRUNCATE TABLE Courses CASCADE;
            TRUNCATE TABLE Students CASCADE;
            TRUNCATE TABLE Enrollments CASCADE;

            -- Insert data into Courses
            INSERT INTO Courses (CourseID, CourseName) VALUES
                (1, 'Mathematics'),
                (2, 'Physics'),
                (3, 'Chemistry');

            -- Insert data into Students
            INSERT INTO Students (StudentID, StudentName) VALUES
                (1, 'Alice'),
                (2, 'Bob'),
                (3, 'Charlie'),
                (4, 'David'),
                (5, 'Eva');

            -- Insert data into Enrollments
            INSERT INTO Enrollments (StudentID, CourseID) VALUES
                (1, 1),
                (2, 1),
                (3, 2),
                (4, 3),
                (5, 1),
                (5, 2);
            """
            )


def get_students() -> list[StudentResult]:
    with pool.connection() as conn:
        # Querying with Pydantic
        with conn.cursor(row_factory=class_row(StudentResult)) as cur:
            cur.execute(
                """
                SELECT * FROM Students;
            """
            )
            students = cur.fetchall()
        return students


def get_students_with_course() -> list[StudentsWithCourseResult]:
    # Querying without Pydantic
    with pool.connection() as conn:
        # Querying with Pydantic
        with conn.cursor(row_factory=class_row(StudentsWithCourseResult)) as cur:
            cur.execute(
                """
                SELECT students.studentname, courses.coursename
                FROM students
                JOIN enrollments
                ON enrollments.studentid=students.studentid
                JOIN courses
                ON courses.courseid=enrollments.courseid;
            """
            )
            students = cur.fetchall()
            return students


initialize_db()
print(get_students())
print(get_students_with_course())
